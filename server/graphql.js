const graphqlHTTP = require('express-graphql');
const axios = require('axios');
const { buildSchema } = require('graphql');

const client = axios.create({
  baseURL: 'https://api.exchangeratesapi.io'
});

const schema = buildSchema(`
  type ExchangeRate {
    id: ID!
    baseCurrency: String!
    currency: String!
    rate: Float!
  }

  type Query {
    currentRates: [ExchangeRate!]!
    historicalRate(date: String!, baseCurrency: String!, currency: String!): ExchangeRate
  }
`);

function normalizeExchangeRateEntry([currency, rate], baseCurrency) {
  return {
    id: [baseCurrency, currency].join('/'),
    baseCurrency,
    currency,
    rate
  };
}

const root = {
  currentRates: async () => {
    const { data: response } = await client.get('/latest', {
      params: { base: 'USD' }
    });
    return Object.entries(response.rates).map(entry =>
      normalizeExchangeRateEntry(entry, response.base)
    );
  },

  historicalRate: async args => {
    const { data: response } = await client.get(`/${args.date}`, {
      params: {
        symbols: args.currency,
        base: args.baseCurrency
      }
    });

    const value = response.rates[args.currency];
    if (!value) {
      return null;
    }

    return normalizeExchangeRateEntry(
      [args.currency, value],
      args.baseCurrency
    );
  }
};

module.exports = graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true
});
